package org.masterupv.estoyaqui.view.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import org.masterupv.estoyaqui.R;
import org.masterupv.estoyaqui.classes.Ubicacion;

public class ConfirmDialogFragmentUbicacion extends DialogFragment implements View.OnClickListener {

    public final static String TAG = ConfirmDialogFragmentUbicacion.class.getCanonicalName();
    private TextView tvTitle, tvMessage;
    private Button btnCancel, btnAccept;
    private final static String ARG_PERSON_MODEL = "arg_person_model";
    private Ubicacion ubicacion;

    // TODO: pass params title and message
    public static ConfirmDialogFragmentUbicacion newInstance(Ubicacion ubicacion) {
        Bundle args = new Bundle();
        args.putParcelable(ARG_PERSON_MODEL, ubicacion);
        ConfirmDialogFragmentUbicacion fragment = new ConfirmDialogFragmentUbicacion();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey(ARG_PERSON_MODEL)) {
            ubicacion = getArguments().getParcelable(ARG_PERSON_MODEL);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.selector_dialog);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_fragment_confirm, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvTitle = view.findViewById(R.id.tv_title);
        tvMessage = view.findViewById(R.id.tv_message);
        btnAccept = view.findViewById(R.id.btn_confirm);
        btnCancel = view.findViewById(R.id.btn_cancel);
        btnAccept.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tvTitle.setText(getString(R.string.title_dialog_delete_ubicacion));
//        String fullname = personModel.getApodo();
//        if (TextUtils.isEmpty(fullname)) {
//            fullname = personModel.getLastname()+" "+personModel.getName();
//        }
        tvMessage.setText(getString(R.string.message_dialog_delete_ubicacion));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:
                dismissAllowingStateLoss();
                break;
            case R.id.btn_confirm:
                dismissAllowingStateLoss();
                if (confirmAction != null) {
                    confirmAction.accept(ubicacion);
                }
                break;
            default:
                break;
        }
    }

    private ConfirmAction confirmAction;

    public interface ConfirmAction {
        void accept(Ubicacion ubicacion);
        void cancel();
    }

    public void setOnConfirmAction(ConfirmAction confirmAction) {
        this.confirmAction = confirmAction;
    }
}
