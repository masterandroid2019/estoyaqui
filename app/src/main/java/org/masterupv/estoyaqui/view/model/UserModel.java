package org.masterupv.estoyaqui.view.model;

import android.os.Parcel;
import android.os.Parcelable;

public class UserModel implements Parcelable {

    private String userId;
    private String photo;
    private String nickname;
    private String name;
    private String lastname;
    private String lastUbicationDescription;
    private double lastUbicationLatitude, lastUbicationLongitude;
    private String lastTimeLocationRefresh;
    private String apodo;

    public UserModel() {
    }

    public UserModel(String userId, String photo, String nickname, String name, String lastname, String lastUbicationDescription,
                     double lastUbicationLatitude, double lastUbicationLongitude, String lastTimeLocationRefresh, String apodo) {
        this.userId = userId;
        this.photo = photo;
        this.nickname = nickname;
        this.name = name;
        this.lastname = lastname;
        this.lastUbicationDescription = lastUbicationDescription;
        this.lastUbicationLatitude = lastUbicationLatitude;
        this.lastUbicationLongitude = lastUbicationLongitude;
        this.lastTimeLocationRefresh = lastTimeLocationRefresh;
        this.apodo = apodo;
    }

    protected UserModel(Parcel in) {
        userId = in.readString();
        photo = in.readString();
        nickname = in.readString();
        name = in.readString();
        lastname = in.readString();
        lastUbicationDescription = in.readString();
        lastUbicationLatitude = in.readDouble();
        lastUbicationLongitude = in.readDouble();
        lastTimeLocationRefresh = in.readString();
        apodo = in.readString();
    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel in) {
            return new UserModel(in);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getLastUbicationDescription() {
        return lastUbicationDescription;
    }

    public void setLastUbicationDescription(String lastUbicationDescription) {
        this.lastUbicationDescription = lastUbicationDescription;
    }

    public double getLastUbicationLatitude() {
        return lastUbicationLatitude;
    }

    public void setLastUbicationLatitude(double lastUbicationLatitude) {
        this.lastUbicationLatitude = lastUbicationLatitude;
    }

    public double getLastUbicationLongitude() {
        return lastUbicationLongitude;
    }

    public void setLastUbicationLongitude(double lastUbicationLongitude) {
        this.lastUbicationLongitude = lastUbicationLongitude;
    }

    public String getLastTimeLocationRefresh() {
        return lastTimeLocationRefresh;
    }

    public void setLastTimeLocationRefresh(String lastTimeLocationRefresh) {
        this.lastTimeLocationRefresh = lastTimeLocationRefresh;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getApodo() {
        return apodo;
    }

    public void setApodo(String apodo) {
        this.apodo = apodo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeString(photo);
        dest.writeString(nickname);
        dest.writeString(name);
        dest.writeString(lastname);
        dest.writeString(lastUbicationDescription);
        dest.writeDouble(lastUbicationLatitude);
        dest.writeDouble(lastUbicationLongitude);
        dest.writeString(lastTimeLocationRefresh);
        dest.writeString(apodo);
    }
}
