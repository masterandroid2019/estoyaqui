package org.masterupv.estoyaqui.view.util;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SpacesLinearItemDecoration extends RecyclerView.ItemDecoration {

    private Context mContext;
    private int externalSpaceDp;

    public SpacesLinearItemDecoration(Context context, int externalSpaceDp) {
        this.externalSpaceDp = ScreenUtils.convertDpToPixel(externalSpaceDp, context);
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        int position = parent.getChildAdapterPosition(view);

        outRect.left = externalSpaceDp;
        outRect.right = externalSpaceDp;
        outRect.bottom = externalSpaceDp/2;
        outRect.top = 0;

        if (position == 0) {
            outRect.top = externalSpaceDp;
        }
    }

}
