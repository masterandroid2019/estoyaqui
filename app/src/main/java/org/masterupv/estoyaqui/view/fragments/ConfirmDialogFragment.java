package org.masterupv.estoyaqui.view.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import org.masterupv.estoyaqui.R;
import org.masterupv.estoyaqui.view.model.UserModel;

public class ConfirmDialogFragment extends DialogFragment implements View.OnClickListener {

    public final static String TAG = ConfirmDialogFragment.class.getCanonicalName();
    private TextView tvTitle, tvMessage;
    private Button btnCancel, btnAccept;
    private final static String ARG_PERSON_MODEL = "arg_person_model";
    private UserModel userModel;

    // TODO: pass params title and message
    public static ConfirmDialogFragment newInstance(UserModel userModel) {
        Bundle args = new Bundle();
        args.putParcelable(ARG_PERSON_MODEL, userModel);
        ConfirmDialogFragment fragment = new ConfirmDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey(ARG_PERSON_MODEL)) {
            userModel = getArguments().getParcelable(ARG_PERSON_MODEL);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.selector_dialog);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_fragment_confirm, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvTitle = view.findViewById(R.id.tv_title);
        tvMessage = view.findViewById(R.id.tv_message);
        btnAccept = view.findViewById(R.id.btn_confirm);
        btnCancel = view.findViewById(R.id.btn_cancel);
        btnAccept.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tvTitle.setText(getString(R.string.title_dialog_delete));
        String fullname = userModel.getApodo();
        if (TextUtils.isEmpty(fullname)) {
            fullname = userModel.getLastname()+" "+ userModel.getName();
        }
        tvMessage.setText(String.format(getString(R.string.message_dialog_delete), fullname));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:
                dismissAllowingStateLoss();
                break;
            case R.id.btn_confirm:
                dismissAllowingStateLoss();
                if (confirmAction != null) {
                    confirmAction.accept(userModel);
                }
                break;
            default:
                break;
        }
    }

    private ConfirmAction confirmAction;

    public interface ConfirmAction {
        void accept(UserModel userModel);
        void cancel();
    }

    public void setOnConfirmAction(ConfirmAction confirmAction) {
        this.confirmAction = confirmAction;
    }
}
