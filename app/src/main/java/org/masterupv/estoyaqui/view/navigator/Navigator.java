package org.masterupv.estoyaqui.view.navigator;

import android.app.ProgressDialog;
import android.content.Context;

import androidx.annotation.IdRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import org.masterupv.estoyaqui.R;
import org.masterupv.estoyaqui.view.fragments.ConfirmDialogFragment;
import org.masterupv.estoyaqui.view.fragments.ContactDetailFragment;
import org.masterupv.estoyaqui.view.fragments.ProgressDialogFragment;
import org.masterupv.estoyaqui.view.fragments.SearchPersonDialogFragment;
import org.masterupv.estoyaqui.view.model.UserModel;

public class Navigator {

    public static void navigateToPersonDetail(FragmentManager fragmentManager, @IdRes int container, int mode, UserModel userModel) {
        if (fragmentManager == null) return;

        fragmentManager.beginTransaction()
                .replace(container, ContactDetailFragment.newInstance(mode, userModel))
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    public static void showSearchDialog(FragmentManager fragmentManager, SearchPersonDialogFragment.SearchPersonInterface searchPersonInterface) {
        if (fragmentManager == null) return;

        Fragment fragment = fragmentManager.findFragmentByTag(SearchPersonDialogFragment.TAG);
        if (fragment == null) {
            fragment = SearchPersonDialogFragment.newInstance();
            ((SearchPersonDialogFragment) fragment).setOnSearchPersonInterface(searchPersonInterface);
            fragmentManager.beginTransaction()
                    .add(fragment, SearchPersonDialogFragment.TAG)
                    .commitAllowingStateLoss();
        }
    }

    public static void showConfirmDialog(FragmentManager fragmentManager, UserModel userModel, ConfirmDialogFragment.ConfirmAction confirmAction) {
        if (fragmentManager == null) return;

        Fragment fragment = fragmentManager.findFragmentByTag(ConfirmDialogFragment.TAG);
        if (fragment == null) {
            fragment = ConfirmDialogFragment.newInstance(userModel);
            ((ConfirmDialogFragment) fragment).setOnConfirmAction(confirmAction);
            fragmentManager.beginTransaction()
                    .add(fragment, ConfirmDialogFragment.TAG)
                    .commitAllowingStateLoss();
        }
    }
    public static void showLoading(FragmentManager fragmentManager) {
        if (fragmentManager == null) return;
        Fragment fragment = fragmentManager.findFragmentByTag(ProgressDialogFragment.TAG);
        if (fragment == null) {
            ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
            progressDialogFragment.setShowsDialog(false);
            progressDialogFragment.setCancelable(false);
            fragmentManager.beginTransaction()
                    .add(progressDialogFragment, ProgressDialogFragment.TAG)
                    .commitAllowingStateLoss();
        }
    }

    public static void dismissLoader(FragmentManager fragmentManager) {
        if (fragmentManager == null) return;
        Fragment f = fragmentManager.findFragmentByTag(ProgressDialogFragment.TAG);
        if (f != null) {
            fragmentManager.beginTransaction()
                    .remove(f)
                    .commitAllowingStateLoss();
        }
    }

    public static void replaceFragment(FragmentManager fragmentManager, @IdRes int container,Fragment fragment) {
        fragmentManager.beginTransaction().replace(container, fragment).commit();
    }

}
