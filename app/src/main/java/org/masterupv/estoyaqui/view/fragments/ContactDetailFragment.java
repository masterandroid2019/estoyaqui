package org.masterupv.estoyaqui.view.fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.google.android.material.appbar.CollapsingToolbarLayout;

import org.masterupv.estoyaqui.R;
import org.masterupv.estoyaqui.data.ConstantService;
import org.masterupv.estoyaqui.data.entity.MapperEntityModelPerson;
import org.masterupv.estoyaqui.data.entity.ResponseEntity;
import org.masterupv.estoyaqui.data.repository.ContactRepository;
import org.masterupv.estoyaqui.data.repository.ContactRepositoryImpl;
import org.masterupv.estoyaqui.view.model.UserModel;
import org.masterupv.estoyaqui.view.navigator.Navigator;
import org.masterupv.estoyaqui.view.util.PreferencesUtil;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ContactDetailFragment extends BaseFragment implements View.OnClickListener {

    private final String TAG = ContactDetailFragment.class.getCanonicalName();
    private final static String ARG_MODE = "arg_mode";
    private final static String ARG_PERSON_MODEL = "arg_person_model";

    public final static int REGISTER_MODE = 1, DETAIL_MODE = 2;
    private int mode = REGISTER_MODE;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private FrameLayout flActionEdit, flActionDelete;
    private LinearLayout llTwoOptions;
    private EditText etNickname, etLastname, etName, etApodo;
    private Button btnAction, btnCancel, btnConfirm;
    private ImageView ivPhoto;
    private UserModel userModel;
    private RelativeLayout rlToolbarOptions;

    private ContactRepository contactRepository;

    public static ContactDetailFragment newInstance(int mode, UserModel userModel) {
        Bundle args = new Bundle();
        args.putInt(ARG_MODE, mode);
        args.putParcelable(ARG_PERSON_MODEL, userModel);
        ContactDetailFragment fragment = new ContactDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contactRepository = new ContactRepositoryImpl(getContext());

        if (getArguments() != null && getArguments().containsKey(ARG_MODE) && getArguments().containsKey(ARG_PERSON_MODEL)) {
            mode = getArguments().getInt(ARG_MODE);
            userModel = getArguments().getParcelable(ARG_PERSON_MODEL);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_person_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rlToolbarOptions = view.findViewById(R.id.rl_toolbar_options);
        collapsingToolbarLayout = view.findViewById(R.id.ctl);
        etNickname = view.findViewById(R.id.et_nickname);
        etLastname = view.findViewById(R.id.et_lastname);
        etName = view.findViewById(R.id.et_name);
        etApodo = view.findViewById(R.id.et_apodo);
        btnAction = view.findViewById(R.id.btn_connect);
        flActionEdit = view.findViewById(R.id.action_edit);
        flActionDelete = view.findViewById(R.id.action_delete);
        llTwoOptions = view.findViewById(R.id.ll_two_options);
        btnCancel = view.findViewById(R.id.btn_cancel);
        btnConfirm = view.findViewById(R.id.btn_confirm);
        ivPhoto = view.findViewById(R.id.iv_photo);
        flActionEdit.setOnClickListener(this);
        flActionDelete.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);
        btnAction.setOnClickListener(this);
    }

    private void setToolbatTitle(String title) {
        collapsingToolbarLayout.setTitle(title);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        collapsingToolbarLayout.setTitleEnabled(true);
        collapsingToolbarLayout.setCollapsedTitleTextColor(ContextCompat.getColor(getContext(), android.R.color.white));
        collapsingToolbarLayout.setExpandedTitleColor(ContextCompat.getColor(getContext(), android.R.color.white));
        etNickname.setText(userModel.getNickname());
        etLastname.setText(userModel.getLastname());
        etName.setText(userModel.getName());
        etApodo.setText(userModel.getApodo());
        ivPhoto.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_person_1));

        if (mode == REGISTER_MODE) {
            setToolbatTitle(getString(R.string.title_register_person));
            llTwoOptions.setVisibility(View.GONE);
            etApodo.setEnabled(true);
            etApodo.requestFocus();
            btnAction.setVisibility(View.VISIBLE);
            rlToolbarOptions.setVisibility(View.GONE);
        } else if (mode == DETAIL_MODE) {
            setToolbatTitle(getString(R.string.title_detail_person));

        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_connect:
                etApodo.setEnabled(false);
                registerPerson();
                btnAction.setVisibility(View.GONE);
                break;
            case R.id.action_edit:
                setToolbatTitle(getString(R.string.title_detail_person));
                rlToolbarOptions.setVisibility(View.GONE);
                etApodo.setEnabled(true);
                etApodo.requestFocus();
                //btnAction.setText(getString(R.string.action_save));
                llTwoOptions.setVisibility(View.VISIBLE);
                break;
            case R.id.action_delete:
                deletePerson();
                break;
            case R.id.btn_cancel:
                rlToolbarOptions.setVisibility(View.VISIBLE);
                etApodo.setText(userModel.getApodo());
                etApodo.setEnabled(false);
                llTwoOptions.setVisibility(View.GONE);
                break;
            case R.id.btn_confirm:
                etApodo.setEnabled(false);
                llTwoOptions.setVisibility(View.GONE);
                rlToolbarOptions.setVisibility(View.VISIBLE);
                updateContact();
                break;
            default:
                break;
        }
    }

    private ConfirmDialogFragment.ConfirmAction confirmDeletePerson = new ConfirmDialogFragment.ConfirmAction() {
        @Override
        public void accept(UserModel userModel) {
            contactRepository.deleteContact(PreferencesUtil.getUser(getContext()).getUserId(), userModel.getUserId())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DeleteContactObserver());
        }

        @Override
        public void cancel() {

        }
    };

    private void deletePerson() {
        Navigator.showConfirmDialog(getFragmentManager(), userModel, confirmDeletePerson);
    }

    private void registerPerson() {
        final String nickname = etNickname.getText().toString();
        final String lastname = etLastname.getText().toString();
        final String name = etName.getText().toString();
        final String apodo = etApodo.getText().toString();

        if (TextUtils.isEmpty(nickname)) {
            showMessage("Ingresa un apodo");
            return;
        }
        if (TextUtils.isEmpty(lastname)) {
            showMessage("Ingresa un apellido");
            return;
        }
        if (TextUtils.isEmpty(name)) {
            showMessage("Ingresa un nombre");
            return;
        }
        if (TextUtils.isEmpty(apodo)) {
            showMessage("Ingresa un apodo");
            return;
        }

        userModel.setNickname(nickname);
        userModel.setLastname(lastname);
        userModel.setName(name);
        userModel.setApodo(apodo);

        contactRepository.addUpdateContact(PreferencesUtil.getUser(getContext()).getUserId(), MapperEntityModelPerson.cast(userModel))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new RegisterContactObserver());

    }

    private void updateContact() {
        final String apodo = etApodo.getText().toString();

        if (TextUtils.isEmpty(apodo)) {
            showMessage("Ingresa un apodo");
            return;
        }

        userModel.setApodo(apodo);

        contactRepository.addUpdateContact(PreferencesUtil.getUser(getContext()).getUserId(), MapperEntityModelPerson.cast(userModel))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new UpdateContactObserver());
    }

    public class RegisterContactObserver implements Observer<ResponseEntity<Boolean>> {

        @Override
        public void onSubscribe(Disposable d) {
            showLoader();
            //Navigator.showLoading(getFragmentManager());
        }

        @Override
        public void onNext(ResponseEntity<Boolean> booleanResponseEntity) {
            if (booleanResponseEntity.getStatus() == ConstantService.SUCCESS_CODE) {
                if (booleanResponseEntity.getResponse()) {
                    ContactListFragment.refreshContacts = true;
                    showMessage("¡Contacto agregado!");
                    rlToolbarOptions.setVisibility(View.VISIBLE);
                }
            }
        }

        @Override
        public void onError(Throwable e) {
            dismissLoader();
            //Navigator.dismissLoader(getFragmentManager());
            processErrorMessage(e);
        }

        @Override
        public void onComplete() {
            dismissLoader();
            //Navigator.dismissLoader(getFragmentManager());
        }
    }

    public class UpdateContactObserver implements Observer<ResponseEntity<Boolean>> {

        @Override
        public void onSubscribe(Disposable d) {
            showLoader();
            //Navigator.showLoading(getFragmentManager());
        }

        @Override
        public void onNext(ResponseEntity<Boolean> booleanResponseEntity) {
            if (booleanResponseEntity.getStatus() == ConstantService.SUCCESS_CODE) {
                if (booleanResponseEntity.getResponse()) {
                    ContactListFragment.refreshContacts = true;
                    showMessage("¡Datos actualizados!");
                    rlToolbarOptions.setVisibility(View.VISIBLE);
                }
            }
        }

        @Override
        public void onError(Throwable e) {
            dismissLoader();
            //Navigator.dismissLoader(getFragmentManager());
            processErrorMessage(e);
        }

        @Override
        public void onComplete() {
            dismissLoader();
            //Navigator.dismissLoader(getFragmentManager());
        }
    }

    public class DeleteContactObserver implements Observer<ResponseEntity<Boolean>> {

        @Override
        public void onSubscribe(Disposable d) {
            showLoader();
            //Navigator.showLoading(getFragmentManager());
        }

        @Override
        public void onNext(ResponseEntity<Boolean> booleanResponseEntity) {
            if (booleanResponseEntity.getStatus() == ConstantService.SUCCESS_CODE) {
                if (booleanResponseEntity.getResponse()) {
                    ContactListFragment.refreshContacts = true;
                    showMessage("¡Contacto eliminado!");
                    if (getActivity() != null) getActivity().onBackPressed();
                }
            }
        }

        @Override
        public void onError(Throwable e) {
            dismissLoader();
            //Navigator.dismissLoader(getFragmentManager());
            processErrorMessage(e);
        }

        @Override
        public void onComplete() {
            dismissLoader();
            //Navigator.dismissLoader(getFragmentManager());
        }
    }
}
