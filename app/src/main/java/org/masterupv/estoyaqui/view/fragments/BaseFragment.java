package org.masterupv.estoyaqui.view.fragments;

import android.widget.Toast;

import androidx.fragment.app.Fragment;

import org.masterupv.estoyaqui.R;
import org.masterupv.estoyaqui.data.exception.NetworkException;
import org.masterupv.estoyaqui.view.navigator.Navigator;

abstract class BaseFragment extends Fragment {

    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void showErrorMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void showErrorNetworkMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    public void processErrorMessage(Throwable t) {
        if (t instanceof NetworkException) {
            showErrorNetworkMessage(getString(R.string.network_error));
        } else {
            showErrorMessage(getString(R.string.generic_error));
        }
    }

    public void showLoader() {
        Navigator.showLoading(getFragmentManager());
    }

    public void dismissLoader() {
        Navigator.dismissLoader(getFragmentManager());
    }
}
