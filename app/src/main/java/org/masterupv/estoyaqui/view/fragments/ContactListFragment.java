package org.masterupv.estoyaqui.view.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.SearchView.OnQueryTextListener;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.masterupv.estoyaqui.R;
import org.masterupv.estoyaqui.data.ConstantService;
import org.masterupv.estoyaqui.data.entity.MapperEntityModelPerson;
import org.masterupv.estoyaqui.data.entity.UserEntity;
import org.masterupv.estoyaqui.data.entity.ResponseEntity;
import org.masterupv.estoyaqui.data.exception.LoginException;
import org.masterupv.estoyaqui.data.exception.UserNotFoundException;
import org.masterupv.estoyaqui.data.repository.ContactRepository;
import org.masterupv.estoyaqui.data.repository.ContactRepositoryImpl;
import org.masterupv.estoyaqui.view.MainActivity;
import org.masterupv.estoyaqui.view.adapter.ContactAdapter;
import org.masterupv.estoyaqui.view.model.UserModel;
import org.masterupv.estoyaqui.view.navigator.Navigator;
import org.masterupv.estoyaqui.view.util.PreferencesUtil;
import org.masterupv.estoyaqui.view.util.SpacesLinearItemDecoration;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ContactListFragment extends BaseFragment implements ContactAdapter.OnItemContactSelect, SearchPersonDialogFragment.SearchPersonInterface {

    private final String TAG = ContactListFragment.class.getCanonicalName();
    private Toolbar mToolbar;
    private RecyclerView rvList;
    private FloatingActionButton fabAdd;
    private OnQueryTextListener onQueryTextListener;
    private ContactAdapter contactAdapter;

    private ContactRepository contactRepository;

    public static boolean refreshContacts;

    public static ContactListFragment newInstance() {
        Bundle args = new Bundle();
        ContactListFragment fragment = new ContactListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contactRepository = new ContactRepositoryImpl(getContext());

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_person_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mToolbar = view.findViewById(R.id.toolbar);
        rvList = view.findViewById(R.id.rv_list);
        fabAdd = view.findViewById(R.id.fab_add);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mToolbar.setTitle(getString(R.string.fragment_persons_title));
        if (getActivity() != null) ((MainActivity) getActivity()).setSupportActionBar(mToolbar);
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigator.showSearchDialog(getFragmentManager(), ContactListFragment.this);
            }
        });
        getContacts();
    }

    private void getContacts() {
        contactRepository.listAllContact(PreferencesUtil.getUser(getContext()).getUserId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ContactListObserver());
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        Log.d(TAG, "called onCreateOptionsMenu");
        menu.clear();
        inflater.inflate(R.menu.person_list_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = new SearchView(((MainActivity) getActivity()).getSupportActionBar().getThemedContext());
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItem.SHOW_AS_ACTION_IF_ROOM);
        item.setActionView(searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                contactAdapter.filterListOfFullname(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.d(TAG, "onQueryTextChange newText: " + newText);
                contactAdapter.filterListOfFullname(newText);
                return false;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void renderPersonList(List<UserModel> userModelList) {
        contactAdapter = new ContactAdapter(getContext(), userModelList);
        contactAdapter.setOnItemContactSelect(this);
        rvList.setAdapter(contactAdapter);
        rvList.setLayoutManager(new LinearLayoutManager(getContext()));
        rvList.addItemDecoration(new SpacesLinearItemDecoration(getContext(), 16));
    }

    @Override
    public void itemContact(UserModel userModel) {
        Navigator.navigateToPersonDetail(getFragmentManager(), R.id.content_view, ContactDetailFragment.DETAIL_MODE, userModel);
    }

    @Override
    public void userSearchByNickname(String nickname) {
        contactRepository.searchContact(nickname)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new UserSearchByNicknameObserver());
    }

    public class UserSearchByNicknameObserver implements Observer<ResponseEntity<UserEntity>> {

        @Override
        public void onSubscribe(Disposable d) {
            showLoader();
            //Navigator.showLoading(getFragmentManager());
        }

        @Override
        public void onNext(ResponseEntity<UserEntity> userEntity) {
            if (userEntity.getStatus() == ConstantService.SUCCESS_CODE) {
                Navigator.navigateToPersonDetail(getFragmentManager(), R.id.content_view,
                        ContactDetailFragment.REGISTER_MODE,
                        MapperEntityModelPerson.cast(userEntity.getResponse()));
            }
        }

        @Override
        public void onError(Throwable e) {
            dismissLoader();
            //Navigator.dismissLoader(getFragmentManager());
            if (e instanceof UserNotFoundException) {
                showMessage("Usuario no encontrado");
            } else {
                processErrorMessage(e);
            }
        }

        @Override
        public void onComplete() {
            dismissLoader();
            //Navigator.dismissLoader(getFragmentManager());
        }
    }

    public class ContactListObserver implements Observer<ResponseEntity<List<UserEntity>>> {

        @Override
        public void onSubscribe(Disposable d) {
            showLoader();
            //Navigator.showLoading(getFragmentManager());
        }

        @Override
        public void onNext(ResponseEntity<List<UserEntity>> personEntityList) {
            renderPersonList(MapperEntityModelPerson.cast(personEntityList.getResponse()));
        }

        @Override
        public void onError(Throwable e) {
            dismissLoader();
            //Navigator.dismissLoader(getFragmentManager());
            processErrorMessage(e);
        }

        @Override
        public void onComplete() {
            dismissLoader();
            //Navigator.dismissLoader(getFragmentManager());
        }
    }

}
