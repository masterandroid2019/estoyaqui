package org.masterupv.estoyaqui.view.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.masterupv.estoyaqui.Adaptors.AdaptadorUbicacion;
import org.masterupv.estoyaqui.R;
import org.masterupv.estoyaqui.classes.Ubicacion;
import org.masterupv.estoyaqui.view.UbicacionActivity;

public class SelectorFragment extends Fragment {

    private Activity actividad;
    private RecyclerView recyclerView;
    private AdaptadorUbicacion adaptador;
    @Override public void onAttach(Activity actividad) {
        super.onAttach(actividad);
        this.actividad = actividad;
        adaptador = new AdaptadorUbicacion(actividad, Ubicacion.ejemploUbicaciones());
    }
    @Override public View onCreateView(LayoutInflater inflador, ViewGroup
            contenedor, Bundle savedInstanceState) {
        final View vista = inflador.inflate(R.layout.ubicacion_selector,
                contenedor, false);

        recyclerView = (RecyclerView) vista.findViewById(
                R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(actividad));
        recyclerView.setAdapter(adaptador);
        adaptador.setOnItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((UbicacionActivity) actividad).mostrarDetalle(recyclerView.getChildAdapterPosition(v));
            }
        });
        return vista;
    }

    public SelectorFragment(){}

    public static SelectorFragment newInstance() {
        return new SelectorFragment();
    }

}
