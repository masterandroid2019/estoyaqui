package org.masterupv.estoyaqui.view.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import org.masterupv.estoyaqui.R;
import org.masterupv.estoyaqui.view.model.UserModel;

import java.util.ArrayList;
import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.MyViewHolder> {

    private Context mContext;
    private List<UserModel> localUserModelList;
    private List<UserModel> userModelList;

    public ContactAdapter(Context context, List<UserModel> userModelList) {
        this.mContext = context;
        this.userModelList = userModelList;
        this.localUserModelList = userModelList;
    }

    public void filterListOfFullname(String filter) {
        filter = filter.toLowerCase();
        userModelList = new ArrayList<>();
        for (UserModel userModel : localUserModelList) {
            if (userModel.getLastname().toLowerCase().contains(filter)
                    || userModel.getName().toLowerCase().contains(filter)
                    || userModel.getApodo().toLowerCase().contains(filter)
                    || userModel.getNickname().toLowerCase().contains(filter)) {
                userModelList.add(userModel);
            }
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.ivPhoto.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_person_1));//userModelList.get(position).getPhoto()
        if (TextUtils.isEmpty(userModelList.get(position).getApodo())) {
            holder.tvFullname.setText(userModelList.get(position).getLastname().concat(" ").concat(userModelList.get(position).getName()));
        } else {
            holder.tvFullname.setText(userModelList.get(position).getApodo());
        }
        holder.tvNickname.setText(userModelList.get(position).getNickname());
    }

    @Override
    public int getItemCount() {
        return userModelList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivPhoto;
        private TextView tvFullname;
        private TextView tvNickname;

        MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            ivPhoto = itemView.findViewById(R.id.iv_photo);
            tvFullname = itemView.findViewById(R.id.tv_fullname);
            tvNickname = itemView.findViewById(R.id.tv_nickname);
            //tvRefreshTime = itemView.findViewById(R.id.tv_time_refresh);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemContactSelect != null) {
                        onItemContactSelect.itemContact(userModelList.get(getAdapterPosition()));
                    }
                }
            });
        }
    }

    private OnItemContactSelect onItemContactSelect;

    public interface OnItemContactSelect {
        void itemContact(UserModel userModel);
    }

    public void setOnItemContactSelect(OnItemContactSelect onItemContactSelect) {
        this.onItemContactSelect = onItemContactSelect;
    }

}
