package org.masterupv.estoyaqui.view.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import org.masterupv.estoyaqui.R;
import org.masterupv.estoyaqui.view.model.UserModel;

public class SearchPersonDialogFragment extends DialogFragment implements View.OnClickListener {

    public final static String TAG = SearchPersonDialogFragment.class.getCanonicalName();
    private EditText etNickname;
    private Button btnSearch;
    private ImageView ivClose;

    public static SearchPersonDialogFragment newInstance() {
        
        Bundle args = new Bundle();
        
        SearchPersonDialogFragment fragment = new SearchPersonDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.selector_dialog);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_fragment_searchperson, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ivClose = view.findViewById(R.id.iv_close);
        etNickname = view.findViewById(R.id.et_nickname);
        btnSearch = view.findViewById(R.id.btn_search);
        ivClose.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_close:
                dismissAllowingStateLoss();
                break;
            case R.id.btn_search:

                if (searchPersonInterface != null) {
                    String nickname = etNickname.getText().toString();
                    if (TextUtils.isEmpty(nickname)) {
                        showMessage("Ingrese un nickname");
                        return;
                    }

                    searchPersonInterface.userSearchByNickname(nickname);
                    dismissAllowingStateLoss();
                    //searchPersonInterface.userSearchByNickname(true, null);
                }
                break;
            default:
                break;
        }
    }

    private SearchPersonInterface searchPersonInterface;

    public interface SearchPersonInterface {
        void userSearchByNickname(String nickname);
    }

    public void setOnSearchPersonInterface(SearchPersonInterface searchPersonInterface) {
        this.searchPersonInterface = searchPersonInterface;
    }

    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }
}
