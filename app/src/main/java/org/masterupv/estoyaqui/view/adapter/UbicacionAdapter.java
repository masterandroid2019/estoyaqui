package org.masterupv.estoyaqui.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import org.masterupv.estoyaqui.R;
import org.masterupv.estoyaqui.classes.Ubicacion;

import java.util.ArrayList;
import java.util.List;

public class UbicacionAdapter extends RecyclerView.Adapter<UbicacionAdapter.MyViewHolder> {

    private Context mContext;
    private List<Ubicacion> localUbicacionList;
    private List<Ubicacion> ubicacionList;

    public UbicacionAdapter(Context context, List<Ubicacion> ubicacionList) {
        this.mContext = context;
        this.ubicacionList = ubicacionList;
        this.localUbicacionList = localUbicacionList;
    }

    public void filterListOfFullname(String filter) {
        filter = filter.toLowerCase();
        ubicacionList = new ArrayList<>();
        for (Ubicacion ubicacion : Ubicacion.ejemploUbicaciones()) {
            if (ubicacion.getAutor().toLowerCase().contains(filter) || ubicacion.getComentario().toLowerCase().contains(filter)) {
                ubicacionList.add(ubicacion);
            }
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ubicacion_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.ivPhoto.setImageDrawable(ContextCompat.getDrawable(mContext, ubicacionList.get(position).getUrlFoto()));
        holder.tvAutor.setText(ubicacionList.get(position).getAutor());
        holder.tvComentario.setText(ubicacionList.get(position).getComentario());
    }

    @Override
    public int getItemCount() {
        return ubicacionList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivPhoto;
        private TextView tvAutor;
        private TextView tvComentario;

        MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            ivPhoto = itemView.findViewById(R.id.iv_photo_ubicacion);
            tvAutor = itemView.findViewById(R.id.tv_autor);
            tvComentario = itemView.findViewById(R.id.tv_comentario);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemUbicacionSelect != null) {
                        onItemUbicacionSelect.item(ubicacionList.get(getAdapterPosition()));
                    }
                }
            });
        }
    }

    private OnItemUbicacionSelect onItemUbicacionSelect;

    public interface OnItemUbicacionSelect {
        void item(Ubicacion ubicacion);
    }

    public void setOnItemUbicacionSelect(OnItemUbicacionSelect onItemUbicacionSelect) {
        this.onItemUbicacionSelect = onItemUbicacionSelect;
    }

}
