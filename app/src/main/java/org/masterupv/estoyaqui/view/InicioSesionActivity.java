package org.masterupv.estoyaqui.view;

import android.app.Activity;
import android.Manifest;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.Sharer;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import org.json.JSONException;
import org.json.JSONObject;
import org.masterupv.estoyaqui.R;
import org.masterupv.estoyaqui.view.model.User;
import org.masterupv.estoyaqui.view.model.UserModel;
import org.masterupv.estoyaqui.view.util.PreferencesUtil;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static org.masterupv.estoyaqui.FirebaseConstants.COLLECTION_USER;

public class InicioSesionActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    FirebaseUser user;
    private ShareDialog elShareDialog;
    private final Activity THIS = this;
    private CallbackManager elCallbackManagerDeFacebook;
    LoginButton loginButtonOficial;
    public static final int SOLICITUD_PERMISO_LOCALIZACION = 0; // TODO
    private FirebaseAnalytics analytics;
    SharedPreferences prefs;
    String vcontraseña;
    String vusuario;
    String vcontraseña1;
    String vusuario1;
    EditText usuario;
    EditText contraseña;
    CheckBox recordarme;
    Boolean vrecordarme;
    String vmail = "";
    String vnombre = "";
    private CallbackManager callbackManager;
    private FirebaseAuth fbAuth;
    private FirebaseAuth.AuthStateListener authListener ;

    private static final int RC_SIGN_IN = 123;
    private GoogleApiClient googleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fbAuth = FirebaseAuth.getInstance();
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        setContentView(R.layout.activity_inicio_sesion);
        loginButtonOficial = (LoginButton) findViewById(R.id.login_button);
        loginButtonOficial.setReadPermissions("email");
        this.elCallbackManagerDeFacebook = CallbackManager.Factory.create();
     //   LoginManager.getInstance().logOut();
// registro un callback para saber cómo ha ido el login
        LoginManager.getInstance().registerCallback(this.elCallbackManagerDeFacebook,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
// App code

                        Toast.makeText(THIS, "Login onSuccess()", Toast.LENGTH_LONG).show();
                        exchangeAccessToken(loginResult.getAccessToken());
                        requestEmail(loginResult.getAccessToken());
                        actualizarVentanita();
                    }
                    @Override
                    public void onCancel() {
                        Toast.makeText(THIS, "Login onCancel()", Toast.LENGTH_LONG).show();
                        actualizarVentanita();
                    }
                    @Override
                    public void onError(FacebookException exception) {
// App code
                        Toast.makeText(THIS, "Login onError(): " + exception.getMessage(),Toast.LENGTH_LONG).show();
                        actualizarVentanita();
                    }
                });


        analytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.METHOD, "oncreate");
        analytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle);
        prefs = getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
        usuario = (EditText) findViewById(R.id.usuario);
        contraseña = (EditText) findViewById(R.id.contraseña);
        recordarme = (CheckBox) findViewById(R.id.recordarme);
    /*    if ( vrecordarme  ){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent );
        }*/
        sha();
        actualizarVentanita();
        Log.d("cuandrav.onCreate", "final .onCreate() ");
        this.elShareDialog = new ShareDialog(this);
        this.elShareDialog.registerCallback(this.elCallbackManagerDeFacebook,
                new FacebookCallback<Sharer.Result>() {
                    @Override
                    public void onSuccess(Sharer.Result result) {
                        Toast.makeText(THIS, "Sharer onSuccess()", Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onCancel() {
                    }
                    @Override
                    public void onError(FacebookException error) {
                        Toast.makeText(THIS, "Sharer onError(): " + error.toString(),
                                Toast.LENGTH_LONG).show();
                    }
                });

//Google
       GoogleSignInOptions gso = new GoogleSignInOptions.Builder(
                GoogleSignInOptions.DEFAULT_SIGN_IN)
                //.requestIdToken("1043041751525-9qt27qun4nc3b1nei0g3fc2f51br1b9u.apps.googleusercontent.com")
                .requestIdToken(getString(R.string.default_web_client_id))

                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        Task<GoogleSignInAccount> task = mGoogleSignInClient.silentSignIn();
        if (task.isSuccessful()) {
            Toast.makeText(THIS, "ya inicio ", Toast.LENGTH_SHORT).show();
        //    mGoogleSignInClient.signOut();
        } else {

        }

        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                 user = fbAuth.getCurrentUser();

                if (user != null){
                    Toast.makeText(getApplicationContext(), "ID FIREBASE ." + fbAuth.getUid()  ,      Toast.LENGTH_SHORT).show();
                   // fbAuth.signOut();
                    accederredes();
                }
            }
        };

        ((ImageView) findViewById(R.id.logo)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserModel userModel = new UserModel();
                userModel.setUserId("C1XVk0BzRMPi155xsftH");
                userModel.setName("william");
                userModel.setLastname("sulca");
                userModel.setNickname("will3");
                userModel.setApodo("will");
                PreferencesUtil.setUser(InicioSesionActivity.this, userModel);
                Intent intent = new Intent(InicioSesionActivity.this, MainActivity.class);
                startActivity(intent, ActivityOptions.
                        makeSceneTransitionAnimation(InicioSesionActivity.this).toBundle());
            }
        });
    }

    private GoogleSignInClient mGoogleSignInClient;

    public void autentificarGoogle(View v) {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    @Override
    public void onStart() {
        super.onStart();
        fbAuth.addAuthStateListener(authListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (authListener != null) {
            fbAuth.removeAuthStateListener(authListener);
        }
    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        Toast.makeText(THIS, " Error de autentificación con Google ",
                Toast.LENGTH_LONG).show();
    }



    @Override
    protected void onActivityResult( int requestCode,  int resultCode,  Intent data) {
// se llama cuando otra actividad que hemos arrancado termina y nos devuelve      el control
// tal vez, devolviéndonos algun resultado (resultCode, data)
        Log.d("cuandrav.onActivityResu", "llamado");
//
// avisar a super
//
        super.onActivityResult(requestCode, resultCode, data);
// google
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
           handleSignInResult(task);
        }


        //
// avisar a Facebook (a su callback manager) por si le afecta
//
        this.elCallbackManagerDeFacebook.onActivityResult(requestCode, resultCode, data);
//        botonLoginTwitter.onActivityResult(requestCode, resultCode, data);


    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {


            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if (completedTask.isSuccessful()) {
                Toast.makeText(THIS, "Autentificación con Google " + account.getEmail()  + account.getDisplayName() ,
                        Toast.LENGTH_LONG).show();
                firebaseauthGoogle(account);
              vnombre = account.getDisplayName();
              vmail = account.getEmail();
                accederredes();
            }else{
                Toast.makeText(THIS, " Error de autentificación con Google "  ,
                        Toast.LENGTH_LONG).show();

            }
            // Signed in successfully, show authenticated UI.




        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(InicioSesionActivity.class.getCanonicalName(), "signInResult:failed code=" + e.getStatusCode());

        }
    }

    private void firebaseauthGoogle(GoogleSignInAccount account) {
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(),null);
        fbAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                prefs =  getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                     editor.putString("IDfire",fbAuth.getUid() );
                editor.commit();
                Toast.makeText(getApplicationContext(), "ID FIREBASE GOOGLE ." + fbAuth.getUid()  ,      Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        vusuario = prefs.getString("usuario", "");
        vrecordarme = prefs.getBoolean("recordarme", false);
        vcontraseña = prefs.getString("contraseña", "");
        if (vrecordarme) {
            acceder(null);
        }

        //PERMISOS
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            solicitarPermiso(Manifest.permission.ACCESS_FINE_LOCATION, "Es necesario los permisos para ubicar su localización", SOLICITUD_PERMISO_LOCALIZACION, this);
        }

    }

    public void loguearCheckbox(View v) {
        CheckBox recordarme = (CheckBox) findViewById(R.id.recordarme);
        String s = "Recordar datos de usuario: " +
                (recordarme.isChecked() ? "Sí" : "No");
        //     Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    public void mostrarContraseña(View v) {

        CheckBox mostrar = (CheckBox) findViewById(R.id.mostrar_contraseña);
        if (mostrar.isChecked()) {
            contraseña.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
            mostrar.setChecked(false);
        } else {
            mostrar.setChecked(true);
            contraseña.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_NORMAL);


        }
    }

    public void accedermail (View view){
        if (vcontraseña.length() > 1 ){
            vcontraseña1 =  contraseña.getText().toString();
            vusuario1 =  usuario.getText().toString();
            if ((vcontraseña.equals( vcontraseña1) ) && (vusuario.equals(vusuario1) )  ){

                if (recordarme.isChecked()) {
                    prefs = getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putBoolean("recordarme", true);
                    editor.commit();
                }

                acceder(null);
            } else {
                Snackbar.make(view, "Usuario y/o clave. No valida ", Snackbar.LENGTH_LONG)
                        .show();
            }
        } else {
            Snackbar.make(view, "Usuario no registrado ", Snackbar.LENGTH_LONG)
                    .show();
        }


    }

    public void accederredes (){
//          prefs =  getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = prefs.edit();
//        editor.putString("usuariofire",vnombre );
//        editor.putString("mailfire",vmail );
//         editor.commit();
//
//        Intent intent = new Intent(this, MainActivity.class);
//        startActivity(intent,
//                ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        acceder(null);
    }

    public void acceder(View view) {
        guardarUsuario(null); // TODO cambiar cuando tengamos respuesta de firestore
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void borrarCampos(View view) {
        usuario.setText("");
        contraseña.setText("");
        usuario.requestFocus();
    }

    public void accederR(View view) {
        String vusuario = usuario.getText().toString();
        if (vusuario.length() > 0) {
            prefs = getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("usuario", vusuario);
            editor.commit();

        }
        Intent intent = new Intent(this, Registro.class);
        startActivity(intent);
    }

    public void sha(){
        try {
            PackageInfo info = this.getPackageManager().getPackageInfo( "org.masterupv.estoyaqui", PackageManager.GET_SIGNATURES);
            for (android.content.pm.Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA"); md.update(signature.toByteArray());
                Log.e("KeyHash", "KeyHash:"+ Base64.encodeToString(md.digest(), Base64.DEFAULT)); }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) { }
    }


    private void actualizarVentanita() {
        Log.e("aquiestoy", "empiezo");
//
// obtengo el access token para ver si hay sesión
//
        AccessToken accessToken = this.obtenerAccessToken();
        if (accessToken == null) {
            Log.e("aquiestoy", "no hay sesion, deshabilito");
//
// sesion con facebook cerrada
//
            return;
        }
//
// sí hay sesión
//   FirebaseUser currentUser = mAuth.getCurrentUser();

        requestEmail(accessToken);

// averiguo los datos básicos del usuario acreditado
//
        Profile profile = Profile.getCurrentProfile();
        if (profile != null) {
            this.usuario.setText(profile.getName());
            vnombre = profile.getName();

        }

        Log.e("aquiestoy", "hay sesion habilito");
        accederredes();
//
// otra forma de averiguar los datos básicos:
// hago una petición con "graph api" para obtener datos del usuario acreditado
//
        this.obtenerPublicProfileConRequest_async(
// como es asíncrono he de dar un callback
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject datosJSON, GraphResponse
                            response) {
//
// muestro los datos
//
                        String nombre= "nombre desconocido";
                        try {
                            nombre = datosJSON.getString("name");
                        } catch (JSONException ex) {
                            Log.d("cuandrav.actualizarVent", "callback de obtenerPublicProfileConRequestasync: excepcion: " + ex.getMessage());
                        } catch (NullPointerException ex) {
                            Log.d("cuandrav.actualizarVent", "callback de obtenerPublicProfileConRequestasync: excepcion: " + ex.getMessage());
                        }
                     //   usuario.setText( nombre);
                    }
                });
    } // ()


    private AccessToken obtenerAccessToken() {
        return AccessToken.getCurrentAccessToken();
    }

    private void obtenerPublicProfileConRequest_async (GraphRequest.GraphJSONObjectCallback
                                                               callback) {
        if (!this.hayRed()) {
            Toast.makeText(this, "¿No hay red?", Toast.LENGTH_LONG).show();
        }
//
// obtengo access token y compruebo que hay sesión
//
        AccessToken accessToken = obtenerAccessToken();
        if (accessToken == null) {


            Toast.makeText(THIS, "no hay sesión con Facebook", Toast.LENGTH_LONG).show();
            return;
        }else {

        }
//
// monto la petición: /me
//
        GraphRequest request = GraphRequest.newMeRequest(accessToken, callback);
        Bundle params = new Bundle ();
        params.putString("fields", "email");

        request.setParameters(params);
//
// la ejecuto (asíncronamente)
//
        request.executeAsync();
    }
    private boolean hayRed() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
// http://stackoverflow.com/questions/15091591/post-on-facebook-wallwithout showing-dialog-on-android
// comprobar que estamos conetactos a internet, antes de hacer el login con
// facebook. Si no: da problemas.
    } // ()




    private void exchangeAccessToken(AccessToken token) {

        AuthCredential credential =
                FacebookAuthProvider.getCredential(token.getToken());

        fbAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (!task.isSuccessful()) {
                            Toast.makeText(getApplicationContext(), "Authentication failed.",      Toast.LENGTH_SHORT).show();
                        }else{
                            prefs =  getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putString("IDfire",fbAuth.getUid() );
                            editor.commit();
                            Toast.makeText(getApplicationContext(), "ID FIREBASE FACEBOOK." + fbAuth.getUid()  ,      Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void requestEmail(AccessToken currentAccessToken) {
        GraphRequest request = GraphRequest.newMeRequest(currentAccessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                if (response.getError() != null) {
                    Toast.makeText(getApplicationContext(), response.getError().getErrorMessage(), Toast.LENGTH_LONG).show();
                    return;
                }
                try {
                    String email = object.getString("email");
     //               setEmail(email);
                    vmail = email;
                    Toast.makeText(getApplicationContext(), "Correo facebook" + email, Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name, last_name, email, gender, birthday, location");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void guardarUsuario(@NonNull final FirebaseUser user) {
        if (user == null) {
            // TODO quitar
            UserModel userModel = new UserModel();
            userModel.setUserId("C1XVk0BzRMPi155xsftH");
            userModel.setName("william");
            userModel.setLastname("sulca");
            userModel.setNickname("will3");
            userModel.setApodo("will");
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            db.collection(COLLECTION_USER).document("12341234").set(userModel);
            PreferencesUtil.setUser(InicioSesionActivity.this, userModel);
        } else {
            UserModel userModel = new UserModel();
            userModel.setUserId(user.getUid());
            userModel.setName(user.getDisplayName());
            userModel.setLastname(user.getEmail());
            userModel.setNickname("");
            userModel.setApodo("");
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            PreferencesUtil.setUser(InicioSesionActivity.this, userModel);
            db.collection(COLLECTION_USER).document(user.getUid()).set(userModel);
        }
    }

    //SOLICITUD DE PERMISOS (INCLUIDO POR MIGUEL)

    public static void solicitarPermiso(final String permiso, String justificacion, final int requestCode, final Activity actividad) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(actividad, permiso)) {
            new AlertDialog.Builder(actividad)
                    .setTitle("Solicitud de permiso")
                    .setMessage(justificacion)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            ActivityCompat.requestPermissions(actividad, new String[]{permiso}, requestCode);
                        }
                    }).show();
        } else {
            ActivityCompat.requestPermissions(actividad, new String[]{permiso}, requestCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == SOLICITUD_PERMISO_LOCALIZACION ) {
            if (grantResults.length == 1 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Sin el permiso, no puedo realizar la acción", Toast.LENGTH_SHORT).show();
                solicitarPermiso(Manifest.permission.ACCESS_FINE_LOCATION, "Es necesario los permisos para ubicar su localización", SOLICITUD_PERMISO_LOCALIZACION, this);
            }
        }
    }
}
