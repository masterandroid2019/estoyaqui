package org.masterupv.estoyaqui.view.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.appbar.CollapsingToolbarLayout;

import org.masterupv.estoyaqui.R;
import org.masterupv.estoyaqui.classes.Ubicacion;
import org.masterupv.estoyaqui.view.navigator.NavigatorUbicacion;

public class UbicacionDetailFragment extends Fragment implements View.OnClickListener {

    private final String TAG = UbicacionDetailFragment.class.getCanonicalName();
    private final static String ARG_MODE = "arg_mode";
    private final static String ARG_PERSON_MODEL = "arg_person_model";

    public final static int REGISTER_MODE = 1, DETAIL_MODE = 2;
    private int mode = REGISTER_MODE;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    //private Toolbar mToolbar;
    private FrameLayout flActionEdit, flActionDelete;
    private LinearLayout llTwoOptions;
    private EditText etAutor, etComentario, etFecha;
    private Button btnAction, btnCancel, btnConfirm;
    private ImageView ivPhoto;
    private Ubicacion ubicacion;
    private RelativeLayout rlToolbarOptions;

    public static UbicacionDetailFragment newInstance(int mode, Ubicacion ubicacion) {
        Bundle args = new Bundle();
        args.putInt(ARG_MODE, mode);
        args.putParcelable(ARG_PERSON_MODEL, ubicacion);
        UbicacionDetailFragment fragment = new UbicacionDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey(ARG_MODE) && getArguments().containsKey(ARG_PERSON_MODEL)) {
            mode = getArguments().getInt(ARG_MODE);
            ubicacion = getArguments().getParcelable(ARG_PERSON_MODEL);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ubicacion_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rlToolbarOptions = view.findViewById(R.id.rl_toolbar_options);
        collapsingToolbarLayout = view.findViewById(R.id.ctl);
        //mToolbar = view.findViewById(R.id.toolbar);
        etAutor = view.findViewById(R.id.et_autor);
        etComentario = view.findViewById(R.id.et_comentario);
        etFecha = view.findViewById(R.id.et_fecha);
        btnAction = view.findViewById(R.id.btn_connect);
        flActionEdit = view.findViewById(R.id.action_edit);
        flActionDelete = view.findViewById(R.id.action_delete);
        llTwoOptions = view.findViewById(R.id.ll_two_options);
        btnCancel = view.findViewById(R.id.btn_cancel);
        btnConfirm = view.findViewById(R.id.btn_confirm);
        ivPhoto = view.findViewById(R.id.iv_photo);
        flActionEdit.setOnClickListener(this);
        flActionDelete.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);
        btnAction.setOnClickListener(this);
    }

    private void setToolbatTitle(String title) {
        collapsingToolbarLayout.setTitle(title);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        collapsingToolbarLayout.setTitleEnabled(true);
        collapsingToolbarLayout.setCollapsedTitleTextColor(ContextCompat.getColor(getContext(), android.R.color.white));
        collapsingToolbarLayout.setExpandedTitleColor(ContextCompat.getColor(getContext(), android.R.color.white));
        etAutor.setText(ubicacion.getAutor());
        etComentario.setText(ubicacion.getComentario());
        etFecha.setText(ubicacion.changeDateFormat(ubicacion.getFecha()));
        ivPhoto.setImageDrawable(ContextCompat.getDrawable(getContext(), ubicacion.getUrlFoto()));

//        if (mode == REGISTER_MODE) {
//            setToolbatTitle(getString(R.string.title_register_person));
//            llTwoOptions.setVisibility(View.GONE);
//            etApodo.setEnabled(true);
//            etApodo.requestFocus();
//            btnAction.setVisibility(View.VISIBLE);
//            rlToolbarOptions.setVisibility(View.GONE);
//        } else if (mode == DETAIL_MODE) {
//            setToolbatTitle(getString(R.string.title_detail_person));
//
//        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.btn_connect:
//                etComentario.setEnabled(false);
//                registerPerson();
//                btnAction.setVisibility(View.GONE);
//                break;
            case R.id.action_edit:
                setToolbatTitle(getString(R.string.title_detail_person));
                rlToolbarOptions.setVisibility(View.GONE);
                etComentario.setEnabled(true);
                etComentario.requestFocus();
                btnAction.setText(getString(R.string.action_save));
                llTwoOptions.setVisibility(View.VISIBLE);
                break;
            case R.id.action_delete:
                deletePerson();
                break;
            case R.id.btn_cancel:
                rlToolbarOptions.setVisibility(View.VISIBLE);
                etComentario.setText(ubicacion.getComentario());
                etComentario.setEnabled(false);
                llTwoOptions.setVisibility(View.GONE);
                break;
            case R.id.btn_confirm:
                etComentario.setEnabled(false);
                llTwoOptions.setVisibility(View.GONE);
                rlToolbarOptions.setVisibility(View.VISIBLE);
//                updateContact();
                break;
            default:
                break;
        }
    }

    private ConfirmDialogFragmentUbicacion.ConfirmAction confirmDeletePerson = new ConfirmDialogFragmentUbicacion.ConfirmAction() {
        @Override
        public void accept(Ubicacion ubicacion) {
            Ubicacion.ejemploUbicaciones().remove(ubicacion);
            if (getActivity() != null) getActivity().onBackPressed();
        }

        @Override
        public void cancel() {

        }
    };

    private void deletePerson() {
        NavigatorUbicacion.showConfirmDialog(getFragmentManager(), ubicacion, confirmDeletePerson);
    }

//    private void registerPerson() {
//        personModel.setApodo(etApodo.getText().toString());
//        LocalData.persons.add(personModel);
//        Toast.makeText(getContext(), "Datos registrados", Toast.LENGTH_SHORT).show();
//        rlToolbarOptions.setVisibility(View.VISIBLE);
//    }

    private void updatePerson() {
        for (int i = 0; i < Ubicacion.ejemploUbicaciones().size(); i++) {
            Ubicacion pm = Ubicacion.ejemploUbicaciones().get(i);
            if (pm.getUbicacionId() == this.ubicacion.getUbicacionId()) {
                pm.setComentario(etComentario.getText().toString());
                Ubicacion.ejemploUbicaciones().set(i, pm);
                Toast.makeText(getContext(), "Datos Actualizados", Toast.LENGTH_SHORT).show();
                break;
            }
        }
    }
}
