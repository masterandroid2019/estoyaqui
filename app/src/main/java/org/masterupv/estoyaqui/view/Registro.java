package org.masterupv.estoyaqui.view;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import org.masterupv.estoyaqui.R;

public class Registro extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        EditText usuario = (EditText) findViewById(R.id.usuarioR);
        EditText contraseña = (EditText) findViewById(R.id.contraseña);
        Button guardar = (Button) findViewById(R.id.guardar);
        SharedPreferences prefs =
                getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);

        String vusuario = prefs.getString("usuario", "");
        if (vusuario.length() > 1 ){
            usuario.setText(vusuario);
            contraseña.requestFocus();
        }

        String vcontraseña = prefs.getString("contraseña", "");
        if (vcontraseña.length() > 1 ){
            contraseña.setText(vcontraseña);
            guardar.requestFocus();
        }


    }

    public void Mcancelar (View view){
        finish();
    }
    public void Mguardar (View view){
        EditText Tcontraseña = (EditText) findViewById(R.id.contraseña);
        EditText Tusuario = (EditText) findViewById(R.id.usuarioR);
        SharedPreferences prefs =
                getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("usuario", Tusuario.getText().toString());
        editor.putString("contraseña", Tcontraseña.getText().toString());

        editor.commit();
        finish();
    }
    public void mostrarContraseñar(View v) {
        EditText contraseña = (EditText) findViewById(R.id.contraseña);
        CheckBox mostrar = (CheckBox) findViewById(R.id.mostrar_contraseña);
        if (mostrar.isChecked()) {
            contraseña.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
            mostrar.setChecked(false);
        } else {
            mostrar.setChecked(true);
            contraseña.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_NORMAL);


        }
    }

}
