package org.masterupv.estoyaqui.view.fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.masterupv.estoyaqui.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SharedFragment extends Fragment {


    public SharedFragment() {
        // Required empty public constructor
    }

    public static Fragment newInstance() {
        return new SharedFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_shared, container, false);
    }

}
