package org.masterupv.estoyaqui.view.navigator;

import androidx.annotation.IdRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import org.masterupv.estoyaqui.classes.Ubicacion;
import org.masterupv.estoyaqui.view.fragments.ConfirmDialogFragment;
import org.masterupv.estoyaqui.view.fragments.ConfirmDialogFragmentUbicacion;
import org.masterupv.estoyaqui.view.fragments.UbicacionDetailFragment;

public class NavigatorUbicacion {

    public static void navigateToUbicacionDetail(FragmentManager fragmentManager, @IdRes int container, int mode, Ubicacion ubicacion) {
        if (fragmentManager == null) return;

        fragmentManager.beginTransaction()
                .replace(container, UbicacionDetailFragment.newInstance(mode, ubicacion))
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

//    public static void showSearchDialog(FragmentManager fragmentManager, SearchPersonDialogFragment.SearchPersonInterface searchPersonInterface) {
//        if (fragmentManager == null) return;
//
//        Fragment fragment = fragmentManager.findFragmentByTag(SearchPersonDialogFragment.TAG);
//        if (fragment == null) {
//            fragment = SearchPersonDialogFragment.newInstance();
//            ((SearchPersonDialogFragment) fragment).setOnSearchPersonInterface(searchPersonInterface);
//            fragmentManager.beginTransaction()
//                    .add(fragment, SearchPersonDialogFragment.TAG)
//                    .commitAllowingStateLoss();
//        }
//    }
//
    public static void showConfirmDialog(FragmentManager fragmentManager, Ubicacion ubicacion, ConfirmDialogFragmentUbicacion.ConfirmAction confirmAction) {
        if (fragmentManager == null) return;

        Fragment fragment = fragmentManager.findFragmentByTag(ConfirmDialogFragment.TAG);
        if (fragment == null) {
            fragment = ConfirmDialogFragmentUbicacion.newInstance(ubicacion);
            ((ConfirmDialogFragmentUbicacion) fragment).setOnConfirmAction(confirmAction);
            fragmentManager.beginTransaction()
                    .add(fragment, ConfirmDialogFragmentUbicacion.TAG)
                    .commitAllowingStateLoss();
        }
    }

}
