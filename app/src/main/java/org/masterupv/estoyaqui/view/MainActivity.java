package org.masterupv.estoyaqui.view;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.masterupv.estoyaqui.R;
import org.masterupv.estoyaqui.view.fragments.MapsFragment;
import org.masterupv.estoyaqui.view.fragments.ContactListFragment;
import org.masterupv.estoyaqui.view.fragments.UbicacionListFragment;
import org.masterupv.estoyaqui.view.navigator.Navigator;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.bnv_maps_option:
                    changeFragment(MapsFragment.newInstance());
                    return true;
                case R.id.bnv_persons_list_option:
                    changeFragment(ContactListFragment.newInstance());
                    return true;
                case R.id.bnv_my_locations_option:
                    changeFragment(UbicacionListFragment.newInstance());
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        changeFragment(MapsFragment.newInstance());
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private void changeFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Navigator.replaceFragment(fragmentManager, R.id.content_view, fragment);
    }

}
