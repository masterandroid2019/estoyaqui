package org.masterupv.estoyaqui.view.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import org.masterupv.estoyaqui.view.model.UserModel;

public class PreferencesUtil {

    private final static String PREFERENCE_NAME = "MisPreferencias";
    private final static String USER_DATA = "user_data";
    private final static int PRIVATE_MODE = 0;
    private final static Gson gson = new Gson();

    public static void setUser(Context context, UserModel userModel) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCE_NAME, PRIVATE_MODE);
        SharedPreferences.Editor editor = preferences.edit();
        String jsonUser = gson.toJson(userModel);
        editor.putString(USER_DATA, jsonUser);
        editor.apply();
    }

    public static UserModel getUser(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCE_NAME, PRIVATE_MODE);
        String json = preferences.getString(USER_DATA, " ");
        if (json.equals(" ") || json.equalsIgnoreCase("null")) {
            return null;
        } else {
            return gson.fromJson(json, UserModel.class);
        }
    }

}
