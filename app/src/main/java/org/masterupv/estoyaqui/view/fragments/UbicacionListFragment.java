package org.masterupv.estoyaqui.view.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.SearchView.OnQueryTextListener;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.masterupv.estoyaqui.R;
import org.masterupv.estoyaqui.classes.Ubicacion;
import org.masterupv.estoyaqui.view.MainActivity;
import org.masterupv.estoyaqui.view.adapter.UbicacionAdapter;
import org.masterupv.estoyaqui.view.navigator.Navigator;
import org.masterupv.estoyaqui.view.navigator.NavigatorUbicacion;
import org.masterupv.estoyaqui.view.util.SpacesLinearItemDecoration;

public class UbicacionListFragment extends Fragment implements UbicacionAdapter.OnItemUbicacionSelect, SearchUbicacionDialogFragment.SearchUbicacionInterface {

    private final String TAG = UbicacionListFragment.class.getCanonicalName();
    private Toolbar mToolbar;
    private RecyclerView rvList;
    private FloatingActionButton fabAdd;
    private OnQueryTextListener onQueryTextListener;
    private UbicacionAdapter ubicacionAdapter;

    public static UbicacionListFragment newInstance() {
        Bundle args = new Bundle();
        UbicacionListFragment fragment = new UbicacionListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_ubicacion_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mToolbar = view.findViewById(R.id.toolbar);
        rvList = view.findViewById(R.id.rv_list);
        fabAdd = view.findViewById(R.id.fab_add);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mToolbar.setTitle(getString(R.string.fragment_ubicacion_title));
        if (getActivity() != null) ((MainActivity)getActivity()).setSupportActionBar(mToolbar);
        getPersons();
//        fabAdd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Navigator.showSearchDialog(getFragmentManager(), UbicacionListFragment.this);
//            }
//        });
    }

    private void getPersons() {
        ubicacionAdapter = new UbicacionAdapter(getContext(), Ubicacion.ejemploUbicaciones());
        ubicacionAdapter.setOnItemUbicacionSelect(this);
        rvList.setAdapter(ubicacionAdapter);
        rvList.setLayoutManager(new LinearLayoutManager(getContext()));
        rvList.addItemDecoration(new SpacesLinearItemDecoration(getContext(), 16));
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        Log.d(TAG, "called onCreateOptionsMenu");
        menu.clear();
        inflater.inflate(R.menu.ubicacion_list_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search_ubicacion);
        SearchView searchView = new SearchView(((MainActivity) getActivity()).getSupportActionBar().getThemedContext());
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItem.SHOW_AS_ACTION_IF_ROOM);
        item.setActionView(searchView);
        searchView.setOnQueryTextListener(new OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                ubicacionAdapter.filterListOfFullname(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.d(TAG, "onQueryTextChange newText: "+newText);
                ubicacionAdapter.filterListOfFullname(newText);
                return false;
            }
        });
    }

    @Override
    public void item(Ubicacion ubicacion) {
        NavigatorUbicacion.navigateToUbicacionDetail(getFragmentManager(), R.id.content_view, UbicacionDetailFragment.DETAIL_MODE, ubicacion);
    }

    @Override
    public void result(boolean found, Ubicacion ubicacion) {
//        if (found) {
//            Navigator.navigateToPersonDetail(getFragmentManager(), R.id.content_view, ContactDetailFragment.REGISTER_MODE, ubicacion);
//        }
    }
}
