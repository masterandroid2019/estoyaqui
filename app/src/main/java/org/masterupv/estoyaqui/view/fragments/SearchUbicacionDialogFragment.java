package org.masterupv.estoyaqui.view.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import org.masterupv.estoyaqui.R;
import org.masterupv.estoyaqui.classes.Ubicacion;

public class SearchUbicacionDialogFragment extends DialogFragment implements View.OnClickListener {

    public final static String TAG = SearchUbicacionDialogFragment.class.getCanonicalName();
    private EditText etNickname;
    private Button btnSearch;
    private ImageView ivClose;

    public static SearchUbicacionDialogFragment newInstance() {
        
        Bundle args = new Bundle();
        
        SearchUbicacionDialogFragment fragment = new SearchUbicacionDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.selector_dialog);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_fragment_searchperson, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ivClose = view.findViewById(R.id.iv_close);
        etNickname = view.findViewById(R.id.et_nickname);
        btnSearch = view.findViewById(R.id.btn_search);
        ivClose.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_close:
                dismissAllowingStateLoss();
                break;
            case R.id.btn_search:
                dismissAllowingStateLoss();
                if (searchUbicacionInterface != null) {
                    Ubicacion ubicacion = new Ubicacion();
//                    personModel.setUserId(LocalData.persons.size()+1);
//                    personModel.setLastname(LocalData.persons.get(0).getLastname());
//                    personModel.setName(LocalData.persons.get(0).getName());
//                    personModel.setLastUbicationDescription(LocalData.persons.get(0).getLastUbicationDescription());
//                    personModel.setNickname(LocalData.persons.get(0).getNickname());
//                    personModel.setPhoto(LocalData.persons.get(0).getPhoto());
//                    personModel.setLastTimeLocationRefresh(LocalData.persons.get(0).getLastTimeLocationRefresh());
                    searchUbicacionInterface.result(true, ubicacion);
                }
                break;
            default:
                break;
        }
    }

    private SearchUbicacionInterface searchUbicacionInterface;

    public interface SearchUbicacionInterface {
        void result(boolean found, Ubicacion ubicacion);
    }

    public void setOnSearchPersonInterface(SearchUbicacionInterface searchPersonInterface) {
        this.searchUbicacionInterface = searchUbicacionInterface;
    }
}
