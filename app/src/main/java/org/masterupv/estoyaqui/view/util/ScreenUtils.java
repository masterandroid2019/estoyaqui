package org.masterupv.estoyaqui.view.util;

import android.content.Context;

public class ScreenUtils {

    public static int convertDpToPixel(float dp, Context context) {
        return (int) (context.getResources().getDisplayMetrics().density * dp);
    }

}
