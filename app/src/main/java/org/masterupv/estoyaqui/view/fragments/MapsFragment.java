package org.masterupv.estoyaqui.view.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.masterupv.estoyaqui.R;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapsFragment extends Fragment implements OnMapReadyCallback, LocationListener {

    private static final String TAG = MapsFragment.class.getSimpleName();
    //Variables para el uso del MAPA
    MapView mapView;
    private static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";
    public double latitude;
    public double longitude;
    @BindView(R.id.maps_tv_address_body)
    TextView tvAddress;
    @BindView(R.id.maps_tv_city_body)
    TextView tvCity;
    @BindView(R.id.maps_tv_country_body)
    TextView tvCountry;
    @BindView(R.id.maps_tv_lat_body)
    TextView tvLat;
    @BindView(R.id.maps_tv_long_body)
    TextView tvLong;
    @BindView(R.id.maps_cardview)
    CardView cardview;
    private LocationManager locationManager;
    public Criteria criteria;
    public String bestProvider;
    GoogleMap mapa;
    private FloatingActionButton fab;

    public MapsFragment() {
        // Required empty public constructor
    }

    public static MapsFragment newInstance() {
        return new MapsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista;
        vista = inflater.inflate(R.layout.fragment_maps, container, false);
        ButterKnife.bind(this, vista);
        mostrarmapa(vista, savedInstanceState);


        return vista;
    }


    private void mostrarmapa(View vista, Bundle savedInstanceState) {
        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }
        mapView = vista.findViewById(R.id.mapView);
        mapView.onCreate(mapViewBundle);
        mapView.getMapAsync(this);

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapa = googleMap;
        //mapa.setMinZoomPreference(20);
        mapa.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        if (ActivityCompat.checkSelfPermission(super.getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED) {
            mapa.setMyLocationEnabled(true);
            mapa.getUiSettings().setZoomControlsEnabled(false); //Desactiva los controles de zoom
            mapa.getUiSettings().setCompassEnabled(false); //Desactiva la brujula
            mapa.getUiSettings().setAllGesturesEnabled(true); //Activa los gestos tactiles sobre el mapa
            mapa.getUiSettings().setMyLocationButtonEnabled(false); //Desactiva el boton de localizar
            getLocation(getContext(), this);

        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle);
        }

        mapView.onSaveInstanceState(mapViewBundle);
    }


    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }


    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }

    @SuppressLint("MissingPermission")
    public void getLocation(Context context, LocationListener listener) {
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        criteria = new Criteria();
        bestProvider = locationManager.getBestProvider(criteria, true);

        Location location = locationManager.getLastKnownLocation(bestProvider);
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            onLocationChanged(location);
        } else {
            locationManager.requestLocationUpdates(bestProvider, 1000, 0, listener);
        }

    }


    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        LatLng latlng = new LatLng(latitude, longitude);
        mapa.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng,20));
        //mapa.addMarker(new MarkerOptions().position(latlng));
        setAddress(latitude, longitude);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    //region Miguel


    //endregion


    //region EDU
    private void setAddress(Double latitude, Double longitude) {

        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(getContext(), Locale.getDefault());

        try {
            cardview.setVisibility(View.VISIBLE);
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                Address address = addresses.get(0);

                String redeableAddress = getRedeableAddress(address);
                String redeableCity = getRedeableCity(address);
                String redeableCountry = getRedeableCountry(address);
                Log.d(TAG, redeableAddress + "\n" + redeableCity + "\n" + redeableCountry);

                tvAddress.setText(redeableAddress);
                tvCity.setText(redeableCity);
                tvCountry.setText(redeableCountry);
                tvLat.setText(String.format(" %s", latitude));
                tvLong.setText(String.format(" %s", longitude));
            }
        } catch (IOException e) {
            e.printStackTrace();
            cardview.setVisibility(View.INVISIBLE);
        }
    }

    private String getRedeableCountry(Address address) {
        StringBuilder stringBuilder = new StringBuilder().append(" ");

        String state = address.getAdminArea();
        String country = address.getCountryName();

        if (country != null) {
            if (state != null) {
                stringBuilder.append(state).append(", ");
            }

            stringBuilder.append(country);
        } else {
            stringBuilder.append(getString(R.string.unknown));
        }
        return stringBuilder.toString();
    }

    private String getRedeableCity(Address address) {
        StringBuilder stringBuilder = new StringBuilder().append(" ");
        String city = address.getLocality();
        String postalCode = address.getPostalCode();
        String subAdminArea = address.getSubAdminArea();

        if (city != null) {
            stringBuilder.append(city);
            if (postalCode != null) {
                stringBuilder.append(" ").append(postalCode);
            }

            if (subAdminArea != null) {
                stringBuilder.append(", ").append(subAdminArea);
            }
        } else {
            stringBuilder.append(getString(R.string.unknown));
        }

        return stringBuilder.toString();
    }

    private String getRedeableAddress(Address address) {
        StringBuilder stringBuilder = new StringBuilder().append(" ");
        String thoroughfare = address.getThoroughfare();
        String subThoroughfare = address.getSubThoroughfare();

        if (thoroughfare != null) {
            stringBuilder.append(thoroughfare);
            if (subThoroughfare != null) {
                stringBuilder.append(", ").append(subThoroughfare);
            }
        } else {
            stringBuilder.append(getString(R.string.unknown));
        }

        return stringBuilder.toString();
    }
    //endregion
}
