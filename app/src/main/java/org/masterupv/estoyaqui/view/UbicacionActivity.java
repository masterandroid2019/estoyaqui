package org.masterupv.estoyaqui.view;

import android.app.FragmentTransaction;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import org.masterupv.estoyaqui.R;
import org.masterupv.estoyaqui.view.fragments.DetalleFragment;
import org.masterupv.estoyaqui.view.fragments.SelectorFragment;

public class UbicacionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubicacion);

        if ((findViewById(R.id.contenedor_pequeno) != null) &&
                (getFragmentManager().findFragmentById(
                        R.id.contenedor_pequeno) == null)) {
            SelectorFragment primerFragment = new SelectorFragment();
            getFragmentManager().beginTransaction()
                    .add(R.id.contenedor_pequeno, primerFragment).commit();
        }
    }

    public void mostrarDetalle(int id) {
        DetalleFragment nuevoFragment = new DetalleFragment();
        Bundle args = new Bundle();
        args.putInt(DetalleFragment.ARG_ID_UBICACION, id);
        nuevoFragment.setArguments(args);
        FragmentTransaction transaccion = getFragmentManager()
                .beginTransaction();
        transaccion.replace(R.id.contenedor_pequeno, nuevoFragment);
        transaccion.addToBackStack(null);
        transaccion.commit();
//        }
    }
}