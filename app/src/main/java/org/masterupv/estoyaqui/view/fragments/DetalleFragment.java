package org.masterupv.estoyaqui.view.fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.masterupv.estoyaqui.R;
import org.masterupv.estoyaqui.classes.Ubicacion;

public class DetalleFragment extends Fragment {

    public static String ARG_ID_UBICACION = "id_ubicacion";

    @Override
    public View onCreateView(LayoutInflater inflador, ViewGroup
            contenedor, Bundle savedInstanceState) {
        View vista = inflador.inflate(R.layout.ubicacion_detalle,
                contenedor, false);
        setHasOptionsMenu(true);
        Bundle args = getArguments();
        if (args != null) {
            int position = args.getInt(ARG_ID_UBICACION);
            ponInfoUbicacion(position, vista);
        } else {
            ponInfoUbicacion(0, vista);
        }
        return vista;
    }

    private void ponInfoUbicacion(int id, View vista) {
        Ubicacion ubicacion = Ubicacion.ejemploUbicaciones().get(id);
        ((TextView) vista.findViewById(R.id.txtComentario)).setText(ubicacion.getComentario());
        ((TextView) vista.findViewById(R.id.txtAutor)).setText(ubicacion.getAutor());
        ((ImageView) vista.findViewById(R.id.fotoUbicacion))
                .setImageResource(ubicacion.getUrlFoto());
        ((TextView) vista.findViewById(R.id.txtFecha)).setText(ubicacion.changeDateFormat(ubicacion.getFecha()));

    }

    public void ponInfoUbicacion(int id) {
        ponInfoUbicacion(id, getView());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu,MenuInflater inflater) {
        inflater.inflate(R.menu.menu_detallefragment, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.photo) {

            return true;
        }
        if (id == R.id.editar) {

            return true;
        }
        if (id == R.id.maps) {

            return true;
        }
        if (id == R.id.guardar) {

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static DetalleFragment newInstance() {
        return new DetalleFragment();
    }

}
