package org.masterupv.estoyaqui.classes;

import android.os.Parcel;
import android.os.Parcelable;

import org.masterupv.estoyaqui.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Ubicacion implements Parcelable {

    private String comentario;
    private int urlFoto;
    private String autor;
    private Double latitud;
    private Double longitud;
    private Long fecha;
    private int ubicacionId;


    public Ubicacion(int ubicacionId, String comentario, int urlFoto, String autor, Double latitud, Double longitud) {
        this.ubicacionId=ubicacionId;
        this.comentario = comentario;
        this.urlFoto = urlFoto;
        this.autor = autor;
        this.latitud = latitud;
        this.longitud = longitud;
        fecha = System.currentTimeMillis();
    }

    public Ubicacion() {
        fecha = System.currentTimeMillis();
    }

    public static final Creator<Ubicacion> CREATOR = new Creator<Ubicacion>() {
        @Override
        public Ubicacion createFromParcel(Parcel in) {
            return new Ubicacion(in);
        }

        @Override
        public Ubicacion[] newArray(int size) {
            return new Ubicacion[size];
        }
    };

    public int getUbicacionId() {
        return ubicacionId;
    }

    public void setUbicacionId(int ubicacionId) {
        this.ubicacionId = ubicacionId;
    }

    public Ubicacion(Parcel in) {
        ubicacionId=in.readInt();
        urlFoto = in.readInt();
        comentario = in.readString();
        autor = in.readString();
        latitud = in.readDouble();
        longitud = in.readDouble();
        fecha = in.readLong();
    }

    //Poner ejemplos de ubicaciones
    public static List<Ubicacion> ejemploUbicaciones() {
        List<Ubicacion> ubicaciones = new ArrayList<Ubicacion>();
        ubicaciones.add(new Ubicacion(1,"Hemos quedado aquí!!!", R.drawable.ejemplo2, "Edu", 48.8667, 2.33333));
        ubicaciones.add(new Ubicacion(2,"Para volver a visitar.", R.drawable.ejemplo3, "William", 40.4167, -3.70325));
        ubicaciones.add(new Ubicacion(3,"Os recomiendo esto.", R.drawable.ejemplo4, "Carlos", -77.0282400, -12.0431800));
        ubicaciones.add(new Ubicacion(4,"Hemos quedado aquí, no? No os veo.", R.drawable.ejemplo5, "Miguel", -0.9862300, 37.6051200));
        ubicaciones.add(new Ubicacion(5,"Interesante volver", R.drawable.ejemplo1, "Gari", 58.97005, 5.73332));
        return ubicaciones;
    }

    public String changeDateFormat(long fecha) {
        Date date = new Date(fecha);
        SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yy");
        String fechaString = df2.format(date);
        return fechaString;
    }


    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public int getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(int urlFoto) {
        this.urlFoto = urlFoto;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public Long getFecha() {
        return fecha;
    }

    public void setFecha(Long fecha) {
        this.fecha = fecha;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ubicacionId);
        dest.writeInt(urlFoto);
        dest.writeString(comentario);
        dest.writeString(autor);
        dest.writeDouble(latitud);
        dest.writeDouble(longitud);
        dest.writeLong(fecha);
    }
}
