package org.masterupv.estoyaqui.Adaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.masterupv.estoyaqui.R;
import org.masterupv.estoyaqui.classes.Ubicacion;

import java.util.List;

public class AdaptadorUbicacion extends RecyclerView.Adapter<AdaptadorUbicacion.ViewHolder> {

    protected List<Ubicacion> listaUbicaciones;
    protected LayoutInflater inflador;
    protected Context contexto;
    protected View.OnClickListener onClickListener;

    public AdaptadorUbicacion(Context contexto, List<Ubicacion> listaUbicaciones){
        this.contexto=contexto;
        this.listaUbicaciones=listaUbicaciones;
        inflador=(LayoutInflater)contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView rvFotoUbicacion;
        public TextView rvAutor, rvComentario, rvFecha;

        public ViewHolder(View itemView) {
            super(itemView);
            rvFotoUbicacion=(ImageView)itemView.findViewById(R.id.rvFotoUbicacion);
            rvAutor = (TextView) itemView.findViewById(R.id.rvAutor);
            rvComentario = (TextView) itemView.findViewById(R.id.rvComentario);
            rvFecha = (TextView) itemView.findViewById(R.id.rvFecha);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = inflador.inflate(R.layout.elemento_ubicacion, viewGroup, false);
        v.setOnClickListener(onClickListener);
        return new AdaptadorUbicacion.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AdaptadorUbicacion.ViewHolder viewHolder, int i) {
        Ubicacion ubicacion = listaUbicaciones.get(i);
        viewHolder.rvFotoUbicacion.setImageResource(ubicacion.getUrlFoto());
        viewHolder.rvAutor.setText(ubicacion.getAutor());
        viewHolder.rvComentario.setText(ubicacion.getComentario());
        viewHolder.rvFecha.setText(ubicacion.changeDateFormat(ubicacion.getFecha()));
    }

    @Override public int getItemCount() {
        return listaUbicaciones.size();
    }

    public void setOnItemClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

}
