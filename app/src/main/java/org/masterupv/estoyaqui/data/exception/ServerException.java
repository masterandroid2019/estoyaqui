package org.masterupv.estoyaqui.data.exception;

/**
 * Created by everis on 2019-06-04.
 */
public class ServerException extends Exception {

    public ServerException() {
    }

    public ServerException(String message) {
        super(message);
    }

}
