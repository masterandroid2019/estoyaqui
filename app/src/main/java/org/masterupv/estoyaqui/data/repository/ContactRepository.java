package org.masterupv.estoyaqui.data.repository;

import org.masterupv.estoyaqui.data.entity.UserEntity;
import org.masterupv.estoyaqui.data.entity.ResponseEntity;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by everis on 2019-06-04.
 */
public interface ContactRepository {

    Observable<ResponseEntity<UserEntity>> searchContact(String nickname);

    Observable<ResponseEntity<Boolean>> addUpdateContact(String myUserId, UserEntity userEntity);

    Observable<ResponseEntity<List<UserEntity>>> listAllContact(String myUserId);

    Observable<ResponseEntity<Boolean>> deleteContact(String myUserId, String userIdRemove);

}
