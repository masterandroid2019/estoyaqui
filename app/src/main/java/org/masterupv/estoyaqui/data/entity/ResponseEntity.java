package org.masterupv.estoyaqui.data.entity;

/**
 * Created by everis on 2019-06-04.
 */
public class ResponseEntity<T> {

    private int status;
    private int errorCode;
    private String message;
    private T response;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }
}
