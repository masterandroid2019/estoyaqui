package org.masterupv.estoyaqui.data.entity;

public class UserDocumentEntity {

    private String userId;
    private Location location;
    private Userinfo userinfo;

    public UserDocumentEntity() {
    }

    public static class Location {
        private String descripton;
        private double latitude;
        private double longitude;
        private String refreshTime;

        public String getDescripton() {
            return descripton;
        }

        public void setDescripton(String descripton) {
            this.descripton = descripton;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public String getRefreshTime() {
            return refreshTime;
        }

        public void setRefreshTime(String refreshTime) {
            this.refreshTime = refreshTime;
        }
    }

    public static class Userinfo {
        private String apodo;
        private String email;
        private String lastname;
        private String name;
        private String nickname;

        public String getApodo() {
            return apodo;
        }

        public void setApodo(String apodo) {
            this.apodo = apodo;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Userinfo getUserinfo() {
        return userinfo;
    }

    public void setUserinfo(Userinfo userinfo) {
        this.userinfo = userinfo;
    }

    public UserEntity castUserEntity() {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserId(getUserId());
        userEntity.setApodo(getUserinfo().getApodo());
        userEntity.setLastname(getUserinfo().getLastname());
        userEntity.setName(getUserinfo().getName());
        userEntity.setNickname(getUserinfo().getNickname());
        userEntity.setLastTimeLocationRefresh(getLocation().getRefreshTime());
        userEntity.setLastUbicationDescription(getLocation().getDescripton());
        userEntity.setLastUbicationLatitude(getLocation().getLatitude());
        userEntity.setLastUbicationLongitude(getLocation().getLongitude());
        return userEntity;
    }

}
