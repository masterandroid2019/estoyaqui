package org.masterupv.estoyaqui.data.exception;

/**
 * Created by everis on 2019-06-04.
 */
public class UserNotFoundException extends Exception {

    public UserNotFoundException() {
    }

    public UserNotFoundException(String message) {
        super(message);
    }

}
