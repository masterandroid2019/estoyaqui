package org.masterupv.estoyaqui.data.repository;

import android.content.Context;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import org.masterupv.estoyaqui.data.entity.ResponseEntity;
import org.masterupv.estoyaqui.data.entity.UserEntity;
import org.masterupv.estoyaqui.data.exception.LoginException;
import org.masterupv.estoyaqui.data.exception.NetworkException;
import org.masterupv.estoyaqui.data.exception.ServerException;
import org.masterupv.estoyaqui.view.util.NetworkUtil;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

import static org.masterupv.estoyaqui.data.ConstantService.SUCCESS_CODE;

public class UserRepositoryImpl implements UserRepository {

    private Context mContext;
    private FirebaseFirestore firebaseFirestore;

    public UserRepositoryImpl(Context mContext) {
        this.mContext = mContext;
        firebaseFirestore = FirebaseFirestore.getInstance();
    }

    @Override
    public Observable<ResponseEntity<UserEntity>> logiUser(final String uid) {
        return Observable.create(new ObservableOnSubscribe<ResponseEntity<UserEntity>>() {
            @Override
            public void subscribe(final ObservableEmitter<ResponseEntity<UserEntity>> subscriber) throws Exception {
                try {
                    if (NetworkUtil.isThereNetworkConnection(mContext)) {
                        final ResponseEntity<UserEntity> responseEntity = new ResponseEntity<>();
                        firebaseFirestore.collection("user").document(uid).get()
                                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                        if (task.isSuccessful()) {
                                            if (task.getResult() != null) {
                                                UserEntity userEntity = task.getResult().toObject(UserEntity.class);
                                                responseEntity.setStatus(SUCCESS_CODE);
                                                responseEntity.setResponse(userEntity);
                                                subscriber.onNext(responseEntity);
                                                subscriber.onComplete();
                                            } else {
                                                subscriber.onError(new LoginException());
                                            }
                                        } else {
                                            subscriber.onError(new ServerException());
                                        }
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        subscriber.onError(new ServerException());
                                    }
                                });
                    } else {
                        subscriber.onError(new NetworkException());
                    }
                } catch (Exception e) {
                    subscriber.onError(new ServerException());
                }
            }
        });
    }

    @Override
    public Observable<ResponseEntity<Boolean>> registerUser(final UserEntity userEntity) {
        return Observable.create(new ObservableOnSubscribe<ResponseEntity<Boolean>>() {
            @Override
            public void subscribe(final ObservableEmitter<ResponseEntity<Boolean>> subscriber) throws Exception {
                try {
                    if (NetworkUtil.isThereNetworkConnection(mContext)) {
                        final ResponseEntity<Boolean> responseEntity = new ResponseEntity<>();
                        firebaseFirestore.collection("user").document(userEntity.getUserId())
                                .set(userEntity.getMapToFireStore())
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            responseEntity.setResponse(true);
                                            subscriber.onNext(responseEntity);
                                            subscriber.onComplete();
                                        } else {
                                            subscriber.onError(new ServerException());
                                        }
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                subscriber.onError(new ServerException());
                            }
                        });
                    } else {
                        subscriber.onError(new NetworkException());
                    }
                } catch (Exception e) {
                    subscriber.onError(new ServerException());
                }
            }
        });
    }
}
