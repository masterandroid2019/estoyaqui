package org.masterupv.estoyaqui.data.exception;

/**
 * Created by everis on 2019-06-04.
 */
public class LoginException extends Exception {

    public LoginException() {
    }

    public LoginException(String message) {
        super(message);
    }

}
