package org.masterupv.estoyaqui.data.repository;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.masterupv.estoyaqui.data.entity.UserDocumentEntity;
import org.masterupv.estoyaqui.data.entity.UserEntity;
import org.masterupv.estoyaqui.data.entity.ResponseEntity;
import org.masterupv.estoyaqui.data.exception.NetworkException;
import org.masterupv.estoyaqui.data.exception.ServerException;
import org.masterupv.estoyaqui.data.exception.UserNotFoundException;
import org.masterupv.estoyaqui.view.util.NetworkUtil;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

import static org.masterupv.estoyaqui.data.ConstantService.SUCCESS_CODE;

/**
 * Created by everis on 2019-06-04.
 */
public class ContactRepositoryImpl implements ContactRepository {

    private final String TAG = ContactRepositoryImpl.class.getCanonicalName();
    private Context mContext;
    private FirebaseFirestore firebaseFirestore;

    public ContactRepositoryImpl(Context mContext) {
        this.mContext = mContext;
        firebaseFirestore = FirebaseFirestore.getInstance();
    }

    @Override
    public Observable<ResponseEntity<UserEntity>> searchContact(final String nickname) {
        return Observable.create(new ObservableOnSubscribe<ResponseEntity<UserEntity>>() {
            @Override
            public void subscribe(final ObservableEmitter<ResponseEntity<UserEntity>> subscriber) throws Exception {
                try {
                    if (NetworkUtil.isThereNetworkConnection(mContext)) {
                        final ResponseEntity<UserEntity> responseEntity = new ResponseEntity<>();
                        firebaseFirestore.collection("user").whereEqualTo("userinfo.nickname", nickname).get()
                                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                        if (task.isSuccessful()) {
                                            if (task.getResult() != null && task.getResult().size() == 1) {
                                                Log.d(TAG, task.getResult().getDocuments().get(0).getData().toString());
                                                UserDocumentEntity userDocumentEntity = task.getResult().getDocuments().get(0).toObject(UserDocumentEntity.class);
                                                userDocumentEntity.setUserId(task.getResult().getDocuments().get(0).getId());
                                                responseEntity.setStatus(SUCCESS_CODE);
                                                responseEntity.setResponse(userDocumentEntity.castUserEntity());
                                                subscriber.onNext(responseEntity);
                                                subscriber.onComplete();
                                            } else {
                                                subscriber.onError(new UserNotFoundException());
                                            }
                                        } else {
                                            subscriber.onError(new ServerException());
                                        }
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        subscriber.onError(new ServerException());
                                    }
                                });
                    } else {
                        subscriber.onError(new NetworkException());
                    }
                } catch (Exception e) {
                    subscriber.onError(new ServerException());
                }
            }
        });
    }

    @Override
    public Observable<ResponseEntity<Boolean>> addUpdateContact(final String myUserId, final UserEntity userEntity) {
        return Observable.create(new ObservableOnSubscribe<ResponseEntity<Boolean>>() {
            @Override
            public void subscribe(final ObservableEmitter<ResponseEntity<Boolean>> subscriber) throws Exception {
                try {
                    if (NetworkUtil.isThereNetworkConnection(mContext)) {
                        final ResponseEntity<Boolean> responseEntity = new ResponseEntity<>();
                        firebaseFirestore.collection("contact").document(myUserId)
                                .collection("users").document(userEntity.getUserId()).set(userEntity.getMapToFireStore())
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            responseEntity.setResponse(true);
                                            subscriber.onNext(responseEntity);
                                            subscriber.onComplete();
                                        } else {
                                            subscriber.onError(new ServerException());
                                        }
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        subscriber.onError(new ServerException());
                                    }
                                });
                    } else {
                        subscriber.onError(new NetworkException());
                    }
                } catch (Exception e) {
                    subscriber.onError(new ServerException());
                }
            }
        });
    }

    @Override
    public Observable<ResponseEntity<List<UserEntity>>> listAllContact(final String myUserId) {
        return Observable.create(new ObservableOnSubscribe<ResponseEntity<List<UserEntity>>>() {
            @Override
            public void subscribe(final ObservableEmitter<ResponseEntity<List<UserEntity>>> subscriber) throws Exception {
                try {
                    if (NetworkUtil.isThereNetworkConnection(mContext)) {
                        final ResponseEntity<List<UserEntity>> responseEntity = new ResponseEntity<>();
                        firebaseFirestore.collection("contact").document(myUserId)
                                .collection("users").get()
                                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                        if (task.isSuccessful() && task.getResult() != null) {
                                            List<UserEntity> userEntityList = new ArrayList<>();
                                            for (QueryDocumentSnapshot queryDocumentSnapshot : task.getResult()) {
                                                UserEntity userEntity = queryDocumentSnapshot.toObject(UserEntity.class);
                                                userEntity.setUserId(queryDocumentSnapshot.getId());
                                                userEntityList.add(userEntity);
                                            }
                                            responseEntity.setStatus(SUCCESS_CODE);
                                            responseEntity.setResponse(userEntityList);
                                            subscriber.onNext(responseEntity);
                                            subscriber.onComplete();
                                        } else {
                                            subscriber.onError(new ServerException());
                                        }
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        subscriber.onError(new ServerException());
                                    }
                                });
                    } else {
                        subscriber.onError(new NetworkException());
                    }
                } catch (Exception e) {
                    subscriber.onError(new ServerException());
                }
            }
        });
    }

    @Override
    public Observable<ResponseEntity<Boolean>> deleteContact(final String myUserId, final String userIdRemove) {
        return Observable.create(new ObservableOnSubscribe<ResponseEntity<Boolean>>() {
            @Override
            public void subscribe(final ObservableEmitter<ResponseEntity<Boolean>> subscriber) throws Exception {
                try {
                    if (NetworkUtil.isThereNetworkConnection(mContext)) {
                        final ResponseEntity<Boolean> responseEntity = new ResponseEntity<>();
                        firebaseFirestore.collection("contact").document(myUserId)
                                .collection("users").document(userIdRemove).delete()
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        responseEntity.setResponse(true);
                                        subscriber.onNext(responseEntity);
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        subscriber.onError(new ServerException());
                                    }
                                });
                    } else {
                        subscriber.onError(new NetworkException());
                    }
                } catch (Exception e) {
                    subscriber.onError(new ServerException());
                }
            }
        });
    }
}
