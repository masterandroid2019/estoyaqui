package org.masterupv.estoyaqui.data.entity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by everis on 2019-06-04.
 */
public class UserEntity {

    private String userId;
    private String photo;
    private String nickname;
    private String name;
    private String lastname;
    private String lastUbicationDescription;
    private double lastUbicationLatitude, lastUbicationLongitude;
    private String lastTimeLocationRefresh;
    private String apodo;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getLastUbicationDescription() {
        return lastUbicationDescription;
    }

    public void setLastUbicationDescription(String lastUbicationDescription) {
        this.lastUbicationDescription = lastUbicationDescription;
    }

    public double getLastUbicationLatitude() {
        return lastUbicationLatitude;
    }

    public void setLastUbicationLatitude(double lastUbicationLatitude) {
        this.lastUbicationLatitude = lastUbicationLatitude;
    }

    public double getLastUbicationLongitude() {
        return lastUbicationLongitude;
    }

    public void setLastUbicationLongitude(double lastUbicationLongitude) {
        this.lastUbicationLongitude = lastUbicationLongitude;
    }

    public String getLastTimeLocationRefresh() {
        return lastTimeLocationRefresh;
    }

    public void setLastTimeLocationRefresh(String lastTimeLocationRefresh) {
        this.lastTimeLocationRefresh = lastTimeLocationRefresh;
    }

    public String getApodo() {
        return apodo;
    }

    public void setApodo(String apodo) {
        this.apodo = apodo;
    }

    public Map<String, Object> getMapToFireStore() {
        Map map = new HashMap();
        map.put("userId", userId);
        map.put("name", name);
        map.put("lastname", lastname);
        map.put("apodo", apodo);
        map.put("nickname", nickname);
        return map;
    }
}
