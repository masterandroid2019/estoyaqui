package org.masterupv.estoyaqui.data.entity;

import org.masterupv.estoyaqui.view.model.UserModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by everis on 2019-06-04.
 */
public class MapperEntityModelPerson {

    public static UserModel cast(UserEntity userEntity) {
        UserModel userModel = new UserModel();
        userModel.setUserId(userEntity.getUserId());
        userModel.setApodo(userEntity.getApodo());
        userModel.setLastname(userEntity.getLastname());
        userModel.setName(userEntity.getName());
        userModel.setNickname(userEntity.getNickname());
        userModel.setLastTimeLocationRefresh(userEntity.getLastTimeLocationRefresh());
        userModel.setLastUbicationDescription(userEntity.getLastUbicationDescription());
        userModel.setLastUbicationLatitude(userEntity.getLastUbicationLatitude());
        userModel.setLastUbicationLongitude(userEntity.getLastUbicationLongitude());
        userModel.setPhoto(userEntity.getPhoto());
        return userModel;
    }

    public static UserEntity cast(UserModel userModel) {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserId(userModel.getUserId());
        userEntity.setApodo(userModel.getApodo());
        userEntity.setLastname(userModel.getLastname());
        userEntity.setName(userModel.getName());
        userEntity.setNickname(userModel.getNickname());
        userEntity.setLastTimeLocationRefresh(userModel.getLastTimeLocationRefresh());
        userEntity.setLastUbicationDescription(userModel.getLastUbicationDescription());
        userEntity.setLastUbicationLatitude(userModel.getLastUbicationLatitude());
        userEntity.setLastUbicationLongitude(userModel.getLastUbicationLongitude());
        userEntity.setPhoto(userModel.getPhoto());
        return userEntity;
    }

    public static List<UserModel> cast(List<UserEntity> userEntityList) {
        List<UserModel> userModelList = new ArrayList<>();
        for (UserEntity userEntity : userEntityList) {
            userModelList.add(cast(userEntity));
        }
        return userModelList;
    }

}
