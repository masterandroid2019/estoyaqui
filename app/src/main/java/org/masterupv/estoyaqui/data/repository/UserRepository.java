package org.masterupv.estoyaqui.data.repository;

import org.masterupv.estoyaqui.data.entity.ResponseEntity;
import org.masterupv.estoyaqui.data.entity.UserEntity;

import io.reactivex.Observable;

public interface UserRepository {

    Observable<ResponseEntity<UserEntity>> logiUser(String uid);

    Observable<ResponseEntity<Boolean>> registerUser(UserEntity userEntity);

}
