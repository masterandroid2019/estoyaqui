package org.masterupv.estoyaqui.data.exception;

/**
 * Created by everis on 2019-06-04.
 */
public class NetworkException extends Exception {

    public NetworkException() {
    }

    public NetworkException(String message) {
        super(message);
    }

}
